package com.ndira.client.portal.tests;

import java.io.IOException;

import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.testng.annotations.Test;
import com.ndira.client.portal.libraries.Loginlogoutmethod;
import com.ndira.client.portal.libraries.Make_a_Rent_or_Loan_to_IRA;
import com.ndira.client.portal.libraries.MyDirectionContributions;
import com.ndira.client.portal.libraries.MyDirectionLoginLogout;
import com.ndira.client.portal.libraries.MyDirectionMessages;
import com.ndira.client.portal.libraries.MyDirectionPortalClientSignupForm;
import com.ndira.client.portal.libraries.MyDirectionPortalReferFriend;
import com.ndira.client.portal.libraries.MyDirection_Invest_PreciousMetals;
import com.ndira.client.portal.libraries.MyDirection_Invest_TransferRollover;
import com.ndira.client.portal.utils.NDira_clientporatal_Constants;
import com.ndira.client.portal.utils.NDira_clientporatal_Propertiesfile;
import com.ndira.client.portal.utils.NDira_clientporatal_XLUtils;

public class NdiraClientPortalTest extends NDira_clientporatal_Constants
{
	NDira_clientporatal_XLUtils xl = new NDira_clientporatal_XLUtils();
	String xlfile = NDira_clientporatal_Propertiesfile.getProperty("file.xlfile.path");
	String tcsheet = "TestCases";
	String tssheet = "TestSteps";
	int tccount, tscount;
	String tcexe;
	String tcid, tsid, keyword;
	String tcres = "";
	boolean res = false;
	Loginlogoutmethod lilom=new Loginlogoutmethod();
	MyDirectionLoginLogout lilo= new MyDirectionLoginLogout();
	MyDirectionMessages messages=new MyDirectionMessages();
	MyDirectionContributions contributions=new MyDirectionContributions();
	MyDirection_Invest_TransferRollover tro=new MyDirection_Invest_TransferRollover();
	MyDirectionPortalReferFriend rf=new MyDirectionPortalReferFriend();
	MyDirectionPortalClientSignupForm clientsignup=new MyDirectionPortalClientSignupForm();
	Make_a_Rent_or_Loan_to_IRA rl=new Make_a_Rent_or_Loan_to_IRA();
	MyDirection_Invest_PreciousMetals preciousmetals=new MyDirection_Invest_PreciousMetals();
	
	@Test
	public void ndira_ClientPortal_Test() throws IOException 
	{
		tccount = xl.getRowCount(xlfile, tcsheet);
		tscount = xl.getRowCount(xlfile, tssheet);
		for (int i = 1; i <= tccount; i++)
		{
			tcexe = xl.getCellData(xlfile, tcsheet, i, 2);
			if (tcexe.equalsIgnoreCase("Y"))
			{
				tcid = xl.getCellData(xlfile, tcsheet, i, 0);
				for (int j = 1; j <= tscount; j++) 
				{
					try
					{
						tsid = xl.getCellData(xlfile, tssheet, j, 0);
					} 
					catch (Exception e) 
					{
						System.out.println("");
					}
					if (tcid.equalsIgnoreCase(tsid)) 
					{
						keyword = xl.getCellData(xlfile, tssheet, j, 4);

						switch (keyword.toUpperCase()) 
						{
						
						case "MYDIRECTIONPORTALLOGINLOGOUT":
							res=lilo.myDirectionLoginLogout();
							break;
						
						case "MYDIRECTIONPORTALMESSAGES":
							messages.subject=xl.getCellData(xlfile, tssheet, j, 5);
							messages.text=xl.getCellData(xlfile, tssheet, j, 6);
							res=messages.mydirectionmessages();
							lilom.mydirectionlogout();
							break;
							
						case "MYDIRECTIONPORTALCONTRIBUTIONS":
							contributions.type=xl.getCellData(xlfile, tssheet, j, 5);
							contributions.bankname=xl.getCellData(xlfile, tssheet, j, 6);
							contributions.amount=xl.getCellData(xlfile, tssheet, j, 7);
							contributions.accounttype=xl.getCellData(xlfile, tssheet, j, 8);
							contributions.accno=xl.getCellData(xlfile, tssheet, j, 9);
							contributions.rootingno=xl.getCellData(xlfile, tssheet, j, 10);
							res=contributions.mydirectioncontributions();
							Sleeper.sleepTightInSeconds(2);
							lilom.mydirectionlogout();
							break;
						
						case "MYDIRECTIONPORTALTRANSFERROLLOVER":
							tro.ssn=xl.getCellData(xlfile, tssheet, j, 5);
							tro.custodianname=xl.getCellData(xlfile, tssheet, j, 6);
							tro.accno=xl.getCellData(xlfile, tssheet, j, 7);
							tro.address=xl.getCellData(xlfile, tssheet, j, 8);
							tro.city=xl.getCellData(xlfile, tssheet, j, 9);
							tro.state=xl.getCellData(xlfile, tssheet, j, 10);
							tro.zip=xl.getCellData(xlfile, tssheet, j, 11);
							tro.name=xl.getCellData(xlfile, tssheet, j, 12);
							tro.pnumber=xl.getCellData(xlfile, tssheet, j, 13);
							tro.fax=xl.getCellData(xlfile, tssheet, j, 14);
							tro.type=xl.getCellData(xlfile, tssheet, j, 15);
							tro.rollover=xl.getCellData(xlfile, tssheet, j, 16);
							tro.accounteligible=xl.getCellData(xlfile, tssheet, j, 17);
							tro.Move=xl.getCellData(xlfile, tssheet, j, 18);
							tro.amount=xl.getCellData(xlfile, tssheet, j, 19);
							tro.Move1=xl.getCellData(xlfile, tssheet, j, 20);
							tro.accountnumber=xl.getCellData(xlfile, tssheet, j, 21);							
							tro.cashamount=xl.getCellData(xlfile, tssheet, j, 22);
							tro.estimatedtotalvalues=xl.getCellData(xlfile, tssheet, j, 23);
							tro.symbol=xl.getCellData(xlfile, tssheet, j, 24);
							tro.sname=xl.getCellData(xlfile, tssheet, j, 25);
							tro.fundn=xl.getCellData(xlfile, tssheet, j, 26);
							tro.fundaccno=xl.getCellData(xlfile, tssheet, j, 27);
							tro.noshares=xl.getCellData(xlfile, tssheet, j, 28);
							tro.Liquidate=xl.getCellData(xlfile, tssheet, j, 29);
							tro.Move2=xl.getCellData(xlfile, tssheet, j, 30);
							tro.Estimatedtotal=xl.getCellData(xlfile, tssheet, j, 31);
							tro.assettype=xl.getCellData(xlfile, tssheet, j, 32);
							tro.nameofasset=xl.getCellData(xlfile, tssheet, j, 33);
							tro.quantity=xl.getCellData(xlfile, tssheet, j, 34);
							tro.value=xl.getCellData(xlfile, tssheet, j, 35);
							
							tro.myfunds=xl.getCellData(xlfile, tssheet, j, 36);
							
							tro.rollover1=xl.getCellData(xlfile, tssheet, j, 37);
							tro.signature=xl.getCellData(xlfile, tssheet, j, 38);
							res=tro.mydirection_invest_transferrollover();
							Sleeper.sleepTightInSeconds(2);
							//lilom.mydirectionlogout();
							break;
							
						case "MYDIRECTIONREFERFRIEND":
							rf.fname=xl.getCellData(xlfile, tssheet, j, 5);
							rf.lname=xl.getCellData(xlfile, tssheet, j, 6);
							rf.reffemail=xl.getCellData(xlfile, tssheet, j, 7);
							res=rf.mydirectionportalreferfriend();
							lilom.mydirectionlogout();
							break;
							
						case "MYDIRECTIONPORTALUSERSIGNUP":
							clientsignup.fname=xl.getCellData(xlfile, tssheet, j, 5);
							clientsignup.lname=xl.getCellData(xlfile, tssheet, j, 6);
							clientsignup.email=xl.getCellData(xlfile, tssheet, j, 7);
							clientsignup.pass=xl.getCellData(xlfile, tssheet, j, 8);
							clientsignup.cpass=xl.getCellData(xlfile, tssheet, j, 9);
							clientsignup.DOB=xl.getCellData(xlfile, tssheet, j, 10);
							clientsignup.SSN=xl.getCellData(xlfile, tssheet, j, 11);
							clientsignup.maritalstatus=xl.getCellData(xlfile, tssheet, j, 12);
							clientsignup.phone=xl.getCellData(xlfile, tssheet, j, 13);
							clientsignup.address1=xl.getCellData(xlfile, tssheet, j, 14);
							clientsignup.address2=xl.getCellData(xlfile, tssheet, j, 15);
							clientsignup.city=xl.getCellData(xlfile, tssheet, j, 16);
							clientsignup.state=xl.getCellData(xlfile, tssheet, j, 17);
							clientsignup.zipcode=xl.getCellData(xlfile, tssheet, j, 18);
							clientsignup.emailnotifications=xl.getCellData(xlfile, tssheet, j, 19);
							clientsignup.typeofaccount=xl.getCellData(xlfile, tssheet, j, 20);
							clientsignup.aaf=xl.getCellData(xlfile, tssheet, j, 21);
							clientsignup.referal=xl.getCellData(xlfile, tssheet, j, 22);
							clientsignup.Coupencode=xl.getCellData(xlfile, tssheet, j, 23);
							clientsignup.Code=xl.getCellData(xlfile, tssheet, j, 24);
							//clientsignup.phonen=xl.getCellData(xlfile, tssheet, j, 23);
							clientsignup.cardno=xl.getCellData(xlfile, tssheet, j, 25);
							clientsignup.cardExpiryDate=xl.getCellData(xlfile, tssheet, j, 26);
							clientsignup.cardName=xl.getCellData(xlfile, tssheet, j, 27);
							clientsignup.cardCVCNumber=xl.getCellData(xlfile, tssheet, j, 28);
							clientsignup.administrationfee=xl.getCellData(xlfile, tssheet, j, 29);
							//clientsignup.AccNo=xl.getCellData(xlfile, tssheet, j, 5);
							res=clientsignup.clientsignupform();
							// d.get(url);
							break;
							
							
						case "MAKEPAYMENTNOW":
							rl.type=xl.getCellData(xlfile, tssheet, j, 5);
							rl.Ownersname=xl.getCellData(xlfile, tssheet, j, 6);
							rl.accno=xl.getCellData(xlfile, tssheet, j, 7);
							rl.address1=xl.getCellData(xlfile, tssheet, j, 8);
							rl.city=xl.getCellData(xlfile, tssheet, j, 9);
							rl.state=xl.getCellData(xlfile, tssheet, j, 10);
							rl.zip=xl.getCellData(xlfile, tssheet, j, 11);
							rl.payeename=xl.getCellData(xlfile, tssheet, j, 12);
							rl.account=xl.getCellData(xlfile, tssheet, j, 13);
							rl.note_details=xl.getCellData(xlfile, tssheet, j, 14);
							rl.principal=xl.getCellData(xlfile, tssheet, j, 15);
							rl.interest=xl.getCellData(xlfile, tssheet, j, 16);
							rl.payer_name=xl.getCellData(xlfile, tssheet, j, 17);
							rl.bankname=xl.getCellData(xlfile, tssheet, j, 18);
							rl.payer_number=xl.getCellData(xlfile, tssheet, j, 19);
							rl.acctnumber=xl.getCellData(xlfile, tssheet, j, 20);
							rl.payer_email=xl.getCellData(xlfile, tssheet, j, 21);
							rl.paymentAmount=xl.getCellData(xlfile, tssheet, j, 22);
							rl.routing=xl.getCellData(xlfile, tssheet, j, 23);
							rl.savingscheck=xl.getCellData(xlfile, tssheet, j, 24);
							rl.comment=xl.getCellData(xlfile, tssheet, j, 25);
							res=rl.PaymenttoIRA();
							d.get(url);
							break;
							
						case "MYDIRECTIONPORTALPRECIOUSMETALS":
							preciousmetals.pay_type=xl.getCellData(xlfile, tssheet, j, 5);
							preciousmetals.dealer=xl.getCellData(xlfile, tssheet, j, 6);
							preciousmetals.depository=xl.getCellData(xlfile, tssheet, j, 7);
							preciousmetals.comment=xl.getCellData(xlfile, tssheet, j, 8);							
							preciousmetals.investinfo=xl.getCellData(xlfile, tssheet, j, 9);
							preciousmetals.Amount=xl.getCellData(xlfile, tssheet, j, 10);
							preciousmetals.transactions=xl.getCellData(xlfile, tssheet, j, 11);
							preciousmetals.invest=xl.getCellData(xlfile, tssheet, j, 12);
							res=preciousmetals.PreciousMetals();
							break;
							

						default:
							break;
						}

						String tsres = null;

						if (res)
						{
							tsres = "Pass";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillGreenColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillGreenColor(xlfile, tcsheet, i, 3);
						} 
						else 
						{
							tsres = "Fail";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillRedColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillRedColor(xlfile, tcsheet, i, 3);
						}
					}
				}
			} 
			else 
			{
				xl.setCellData(xlfile, tcsheet, i, 3, "Not Executed");
				xl.fillOrangeColor(xlfile, tcsheet, i, 3);
				
			}
		}
	}

}
