package com.ndira.client.portal.libraries;

import org.smslib.AGateway;
import org.smslib.IInboundMessageNotification;
import org.smslib.IOutboundMessageNotification;
import org.smslib.InboundMessage;
import org.smslib.Message;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;


public class Smslib {

 /**
  * @param args the command line arguments
  */
 public static void main(String[] args) {
  Smslib app = new Smslib();
        try {
            app.sendMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
 }
 
   public void sendMessage() throws Exception {

        SerialModemGateway gateway = new SerialModemGateway("modem.com1", "COM1", 9600, "OPPO", "COM1");
        gateway.setInbound(true);
        gateway.setOutbound(true);

        OutboundNotification outboundNotification = new OutboundNotification();
        InboundNotification inboundNotification = new InboundNotification();

        Service service = Service.getInstance();
        service.setOutboundMessageNotification(outboundNotification);
        service.setInboundMessageNotification(inboundNotification);
        service.addGateway(gateway);
        service.startService();

        OutboundMessage msg = new OutboundMessage("+919700378189", "test");
        service.sendMessage(msg);
//  service.stopService();
    }

    public class InboundNotification implements IInboundMessageNotification {
        @Override
//Get triggered when a SMS is received
        public void process(AGateway gateway, Message.MessageTypes messageTypes, InboundMessage inboundMessage) {

            System.out.println(inboundMessage);
            try {
//                gateway.deleteMessage(inboundMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class OutboundNotification implements IOutboundMessageNotification {

//Get triggered when a SMS is sent
  @Override
        public void process(AGateway gateway, OutboundMessage outboundMessage) {
            System.out.println(outboundMessage);
        }
    }
 
}