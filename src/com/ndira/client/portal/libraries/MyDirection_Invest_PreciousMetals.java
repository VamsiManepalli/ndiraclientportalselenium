package com.ndira.client.portal.libraries;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.client.portal.utils.NDira_clientporatal_Constants;

public class MyDirection_Invest_PreciousMetals extends NDira_clientporatal_Constants
{
	public static String pay_type,dealer,depository,comment,Amount,
	investinfo,transactions,invest;
	public static boolean PreciousMetals() throws IOException
	{
		Loginlogoutmethod lilo=new Loginlogoutmethod();
		lilo.mydirectionLogin();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[4]/div[1]/ul/li[5]/a/span[1]")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='menu']/li[5]/ul/li[7]/a")).click();
		Sleeper.sleepTightInSeconds(2);
		//Select an account & action
		
		Select account=new Select(d.findElement(By.id("accounts")));
		account.selectByIndex(1);
		Sleeper.sleepTightInSeconds(2);
		Select paytype=new Select(d.findElement(By.id("pmtype")));
		paytype.selectByVisibleText(pay_type);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("dealer")).sendKeys(dealer);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("depo")).sendKeys(depository);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("check_comment")).sendKeys(comment);
		
		//Investment Information
		if (investinfo.equalsIgnoreCase("Allocate the dollar amount stated below for my precious metals purchase.")) 
		{
			Sleeper.sleepTightInSeconds(2);
			//d.findElement(By.xpath(".//*[@id='InvestmentInfo']/div[1]/div/div[1]/label/span")).click();
			d.findElement(By.id("paymentAmount")).sendKeys(Amount);
		} 
		else
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath(".//*[@id='InvestmentInfo']/div[3]/div/label/span")).click();
		}
		//How would you like to pay for this transaction?
		if (transactions.equalsIgnoreCase("Call me to pay with all major credit cards."))
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath(".//*[@id='precious_metal_investment']/div[13]/div/div/label/span")).click();
		}
		//acknowledge
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='precious_metal_investment']/div[15]/div/div[1]/label/span[1]")).click();
		if (invest.equalsIgnoreCase("Yes, I would like a New Direction IRA representative to give me a courtesy call and inform me when my investment will be funded. (Please note that this will not hold up the funding process)")) 
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath(".//*[@id='call-label']/span")).click();
		}
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='precious_metal_investment']/div[16]/div/div/div/div[1]/button")).click();
		
		String expmsg,acmsg;
		expmsg=d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[3]/div/div[2]/h4")).getText();
		acmsg="THANK YOU";
		if (expmsg.equals(acmsg)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
	}
}
