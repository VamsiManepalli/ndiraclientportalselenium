package com.ndira.client.portal.libraries;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.client.portal.utils.NDira_clientporatal_Constants;

public class Make_a_Rent_or_Loan_to_IRA extends NDira_clientporatal_Constants
{
	public static String type,Ownersname,accno,address1,city,state,zip,payeename,account,note_details,principal,interest,payer_name,
	bankname,payer_number,acctnumber,payer_email,paymentAmount,routing,savingscheck,comment;
	public static boolean PaymenttoIRA()
	{
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='payment']/div[1]/a")).click();
		Sleeper.sleepTightInSeconds(2);
		Select Transactiontype=new Select(d.findElement(By.id("ticket_type")));
		Transactiontype.selectByVisibleText(type);
		Sleeper.sleepTightInSeconds(2);
		
		if (type.equalsIgnoreCase("Rent Payment"))
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("payeename2")).sendKeys(Ownersname);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("account2")).sendKeys(accno);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("address1")).sendKeys(address1);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("city")).sendKeys(city);
			Sleeper.sleepTightInSeconds(2);
			Select statedrop=new Select(d.findElement(By.id("state")));
			statedrop.selectByVisibleText(state);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("zip")).sendKeys(zip);
		} 
		else if (type.equalsIgnoreCase("Note Payment")) 
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("payeename")).sendKeys(payeename);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("account")).sendKeys(account);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("note_details")).sendKeys(note_details);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("principal")).sendKeys(principal);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("interest")).sendKeys(interest);
			
		}
		else
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("payeename2")).sendKeys(Ownersname);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("account2")).sendKeys(accno);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("address1")).sendKeys(address1);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("city")).sendKeys(city);
			Sleeper.sleepTightInSeconds(2);
			Select statedrop=new Select(d.findElement(By.id("state")));
			statedrop.selectByVisibleText(state);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("zip")).sendKeys(zip);
		}
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("payer_name")).sendKeys(payer_name);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("bankname")).sendKeys(bankname);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("payer_number")).clear();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("payer_number")).sendKeys(payer_number);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("acctnumber")).sendKeys(acctnumber);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("payer_email")).sendKeys(payer_email);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("acctnumber2")).sendKeys(acctnumber);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("paymentAmount")).sendKeys(paymentAmount);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("routing")).sendKeys(routing);
		Sleeper.sleepTightInSeconds(2);
		Select bankacctype=new Select(d.findElement(By.id("savingscheck")));
		bankacctype.selectByVisibleText(savingscheck);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("comment")).sendKeys(comment);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("submit-button")).click();
		
		String expmsg;
		expmsg=d.findElement(By.xpath("html/body/div[2]/div[4]/h4")).getText();
		if (expmsg.contains("THANK YOU"))
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
	}
}
