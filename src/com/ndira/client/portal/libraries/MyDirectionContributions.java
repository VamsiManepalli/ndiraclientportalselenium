package com.ndira.client.portal.libraries;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.ndira.client.portal.utils.NDira_clientporatal_Constants;

public class MyDirectionContributions extends NDira_clientporatal_Constants
{	public static String type,bankname,amount,accounttype,accno,rootingno;
	public static boolean mydirectioncontributions() throws IOException 
	{
		try 
		{
			Loginlogoutmethod lilo=new Loginlogoutmethod();
			lilo.mydirectionLogin();
		//	lilo.mydirectionsecurityquestion();
			Sleeper.sleepTightInSeconds(5);
			d.findElement(By.xpath("html/body/div[4]/div[1]/ul/li[3]/a/span")).click();
			Sleeper.sleepTightInSeconds(2);
			Select contributiontype=new Select(d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[4]/div/div/div[1]/form/div[3]/select")));
			contributiontype.selectByVisibleText(type);
			d.findElement(By.id("bankname")).sendKeys(bankname);
			d.findElement(By.id("paymentAmount")).sendKeys(amount);
			Sleeper.sleepTightInSeconds(2);
			Select acctype=new Select(d.findElement(By.id("savingscheck")));
			acctype.selectByVisibleText(accounttype);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("acctnumber")).sendKeys(accno);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("acctnumber2")).sendKeys(accno);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("routing")).sendKeys(rootingno);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[4]/div/div/div[1]/form/div[13]/label/span")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[4]/div/div/div[1]/form/div[16]/div/div/div/div[1]/button")).click();
			String expmsg;
			expmsg=d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[4]/div/div/div[1]/form/div[1]/div[1]")).getText();
			if (expmsg.contains("Success"))
			{
				//lilo.mydirectionlogout();
				return true;
			} 
			else
			{
				//lilo.mydirectionlogout();
				return false;
			}
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
		
	}
}
