package com.ndira.client.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import com.ndira.client.portal.utils.NDira_clientporatal_Constants;


public class MyDirectionLoginLogout extends NDira_clientporatal_Constants
{
	public static boolean myDirectionLoginLogout() throws IOException
	{
		try 
		{
		Loginlogoutmethod lilo=new Loginlogoutmethod();
		lilo.mydirectionLogin();
		
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		lilo.mydirectionlogout();
		
		String expurl;
		expurl=d.getCurrentUrl();
		if (expurl.contains(url)) 
		{
			return true;
		} 
		else
		{
			return false;
		}
	}
	catch (Exception e)
	{
		System.out.println(e);
		return false;
	}
		
	}
}
