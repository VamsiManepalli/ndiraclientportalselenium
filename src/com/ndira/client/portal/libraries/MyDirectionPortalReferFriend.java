package com.ndira.client.portal.libraries;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.client.portal.utils.NDira_clientporatal_Constants;

public class MyDirectionPortalReferFriend extends NDira_clientporatal_Constants
{
	public static String fname, lname, reffemail;
	public static boolean mydirectionportalreferfriend() throws IOException 
	{
		try 
		{
		Loginlogoutmethod lilo=new Loginlogoutmethod();
		lilo.mydirectionLogin();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div[4]/a[7]")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("referfirstname")).sendKeys(fname);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("referlastname")).sendKeys(lname);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("referemail")).sendKeys(reffemail);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[2]/div[3]/div[2]/div/form/div[2]/div[1]/div/button")).click();
		Sleeper.sleepTightInSeconds(2);
		String expmsg;
		expmsg=d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[2]/div[3]")).getText();
		if (expmsg.contains("Success"))
		{
			return true;
		}
		else 
		{
			return false;
		}
	} 
	catch (Exception e)
	{
		System.out.println(e);
		return false;
	}
		
  }
}
