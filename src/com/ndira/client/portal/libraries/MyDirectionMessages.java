package com.ndira.client.portal.libraries;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.client.portal.utils.NDira_clientporatal_Constants;
import com.ndira.client.portal.utils.NDira_clientporatal_Propertiesfile;

public class MyDirectionMessages extends NDira_clientporatal_Constants
{
	
	public static String subject,account,text;
	public boolean mydirectionmessages() throws IOException 
	{
		try 
		{
		Loginlogoutmethod lilo=new Loginlogoutmethod();
		lilo.mydirectionLogin();
		//lilo.mydirectionsecurityquestion();
		
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[3]/div[3]/ul/li[1]/a")).click();
		Sleeper.sleepTightInSeconds(2);
		Select subdropdown=new Select(d.findElement(By.id("subject_dropdown")));
		subdropdown.selectByVisibleText(subject);
		Sleeper.sleepTightInSeconds(2);
		if (subject.contains("Account Specific Questions"))
		{
			Select acc=new Select(d.findElement(By.id("account_dropdown")));
			acc.selectByIndex(1);
		}
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("comments_area")).sendKeys(text);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("attachment_file")).click();
		Sleeper.sleepTightInSeconds(2);
		String windows_component = NDira_clientporatal_Propertiesfile.getProperty("file.windows.component");
		Runtime.getRuntime().exec(windows_component);
		d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div/div[3]/div/form/div[5]/div/div/div[1]/div/input")).click();
		Sleeper.sleepTightInSeconds(2);
		String exp;
		exp=d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div/div[3]/div/div[2]/table/tbody/tr[1]/td[1]")).getText();
		if (exp.contains(subject)) 
		{
			return true;	
		}
		else
		{
			return false;
		}
	  } 
	  catch (Exception e) 
	  {
		System.out.println(e);
		return false;
	  }
	}
}
