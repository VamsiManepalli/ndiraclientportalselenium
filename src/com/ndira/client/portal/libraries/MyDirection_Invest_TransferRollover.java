package com.ndira.client.portal.libraries;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.client.portal.utils.NDira_clientporatal_Constants;
import com.ndira.sponsor.portal.utils.NdiraUtils;

public class MyDirection_Invest_TransferRollover extends NDira_clientporatal_Constants
{
	public static String ssn,custodianname,accno,address,city,state,zip,name,pnumber,fax,type,rollover,accounteligible,assets,
	Move,amount,accountnumber,Move1,cashamount,estimatedtotalvalues,symbol,sname,fundn,fundaccno,noshares,Liquidate,
	Move2,Estimatedtotal,
	myfunds,rollover1,signature,
	assettype,nameofasset,quantity,value;
	public static boolean mydirection_invest_transferrollover() throws IOException 
	{
		try
		{
		Loginlogoutmethod lilo=new Loginlogoutmethod();
		lilo.mydirectionLogin();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[4]/div[1]/ul/li[5]/a/span[1]")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[4]/div[1]/ul/li[5]/ul/li[1]/a")).click();
		Sleeper.sleepTightInSeconds(2);
		
		//Account Information
		Sleeper.sleepTightInSeconds(2);
		Select account=new Select(d.findElement(By.id("accountName")));
		account.selectByIndex(1);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("socialSecurity")).sendKeys(ssn);
		Sleeper.sleepTightInSeconds(2);
		
		//Where are these assets currently?
		d.findElement(By.id("custodianName")).sendKeys(custodianname);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("custodianAccountNumber")).sendKeys(accno);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("custodianAccountStreetAddressStreet")).sendKeys(address);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("custodianAccountStreetAddressCity")).sendKeys(city);
		Sleeper.sleepTightInSeconds(2);
		Select dstate=new Select(d.findElement(By.id("custodianAccountStreetAddressState")));
		dstate.selectByVisibleText(state);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("custodianAccountStreetAddressZip")).sendKeys(zip);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("custodianContactName")).sendKeys(name);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("custodianPhoneNumber")).sendKeys(pnumber);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.id("custodianFaxNumber")).sendKeys(fax);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[3]/div[11]/div[2]/div/div/div[1]/input")).click();
		Sleeper.sleepTightInSeconds(2);
		String windows_component = NdiraUtils.getProperty("file.windows.component");
		Runtime.getRuntime().exec(windows_component);
		Sleeper.sleepTightInSeconds(2);
		
		
		//Transfer or Rollover
		Select TransferorRolloverFrom=new Select(d.findElement(By.id("transferOrRolloverSelectedAccountName")));
		TransferorRolloverFrom.selectByVisibleText(type);
		Sleeper.sleepTightInSeconds(2);
		//String ab=d.findElement(By.id("transferOrRolloverSelectedAccountName")).getText();
		//d.findElement(By.xpath(".//*[@id='rolloverPastDaysDisplay']/div/div[1]/label/span")).click();
		
		if (type.equalsIgnoreCase("Traditional IRA"))
		{
			if (rollover.equals("Yes"))
			{
				Sleeper.sleepTightInSeconds(4);
				d.findElement(By.xpath(".//*[@id='rolloverPastDaysDisplay']/div/div[1]/label/span")).click();
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferEligible']/label/span")).click();
			} 
			else
			{
				Sleeper.sleepTightInSeconds(5);
				d.findElement(By.xpath(".//*[@id='rolloverPastDaysDisplay']/div/div[2]/label/span")).click();
				Sleeper.sleepTightInSeconds(2);
				if (rollover.equals("Transfer"))
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferEligible']/label/span")).click();
				} 
				else 
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='rolloverEligible']/label/span")).click();		
				}
						
			}
		}
		if (type.equalsIgnoreCase("SIMPLE IRA"))
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath(".//*[@id='afterTwoYearsMessage']/label/span")).click();
			
			if (rollover.equals("Yes"))
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='rolloverPastDaysDisplay']/div/div[1]/label/span")).click();
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferEligible']/label/span")).click();
			} 
			else
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='rolloverPastDaysDisplay']/div/div[2]/label/span")).click();
				if (accounteligible.equals("Transfer"))
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferEligible']/label/span")).click();
				} 
				else 
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='rolloverEligible']/label/span")).click();
				}
			}
		}
		if (type.equalsIgnoreCase("Governmental 457(b)"))
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[4]/div[10]/div/div[2]/label/span")).click();
		}
		if (type.equalsIgnoreCase("Qualified Plan (pre-tax; profit-sharing, 401(k), money purchase, and defined benefit plans)"))
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[4]/div[10]/div/div[2]/label/span")).click();
		}
		if (type.equalsIgnoreCase("Designated Roth Account (401(k), 403(b), or 457(b))"))
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[4]/div[10]/div/div[2]/label/span")).click();
		}
		if (type.equalsIgnoreCase("403(b) (pre-tax)"))
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[4]/div[10]/div/div[2]/label/span")).click();
		}
		
		//Assets to be moved to NDIRA
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='transferRolloverAsset']/div[3]/label/span")).click();
		if (Move.equals("Move Entire")) 
		{
			Sleeper.sleepTightInSeconds(4);
			d.findElement(By.xpath(".//*[@id='transferRolloverAssetCashMoneyMarketInfo']/div[2]/div[1]/label/span")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("transferRolloverAssetCashMoneyMarketMoveEntireAmount")).sendKeys(amount);
		} 
	   else 
		{
			Sleeper.sleepTightInSeconds(4);
			d.findElement(By.xpath(".//*[@id='transferRolloverAssetCashMoneyMarketInfo']/div[2]/div[3]/label/span")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("transferRolloverAssetCashMoneyMarketMovePartialAmount")).sendKeys(amount);
		}
		
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='transferRolloverAsset']/div[5]/label/span")).click();
		if (Move1.equals("Move Entire Account")) 
			{
				Sleeper.sleepTightInSeconds(4);
				d.findElement(By.xpath(".//*[@id='transferRolloverAssetBrokerageInfo']/div/div[1]/label/span")).click();
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferRolloverAssetBrokerageMoveEntireAccountNumber']")).sendKeys(accountnumber);
			}
			else 
			{
				Sleeper.sleepTightInSeconds(4);
				d.findElement(By.xpath(".//*[@id='transferRolloverAssetBrokerageInfo']/div/div[3]/label/span")).click();
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.id("transferRolloverAssetBrokerageMovePartialAccountNumber")).sendKeys(accountnumber);
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.id("transferRolloverAssetBrokerageMovePartialCashAmount")).sendKeys(cashamount);
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.id("transferRolloverAssetBrokerageMovePartialEstimatedValue")).sendKeys(estimatedtotalvalues);
				//Brokerage account assets information:
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferRolloverAssetBrokerageMovePartialDiv']/div[5]/div[2]/div/div/div[1]/div[1]/input")).sendKeys(symbol);
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[5]/div[6]/div/div[5]/div[5]/div[2]/div/div/div[1]/div[2]/input")).sendKeys(fundn);
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[5]/div[6]/div/div[5]/div[5]/div[2]/div/div/div[1]/div[3]/input")).sendKeys(fundaccno);
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[5]/div[6]/div/div[5]/div[5]/div[2]/div/div/div[1]/div[4]/input")).sendKeys(noshares);
				
				if (Liquidate.equals("Yes"))
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[5]/div[6]/div/div[5]/div[5]/div[2]/div/div/div[1]/div[6]/label[1]/span")).click();
				}
				else
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[5]/div[6]/div/div[5]/div[5]/div[2]/div/div/div[1]/div[6]/label[2]/span")).click();
				}
			}
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='transferRolloverAsset']/div[7]/label/span")).click();
		if (Move2.equals("Complete movement of cash and/or assets to NDIRA"))
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeInfo']/div[2]/div[1]/label/span")).click();
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMoveEntireDiv']/div[1]/div[1]/label/span")).click();
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMoveEntireDiv']/div[1]/div[2]/label/span")).click();
				} 
				else 
				{
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeInfo']/div[2]/div[3]/label/span")).click();
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.id("transferRolloverAssetAlternativeMovePartialEstimatedValue")).sendKeys(Estimatedtotal);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMovePartialDiv']/div[2]/div[1]/label/span")).click();
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMovePartialDiv']/div[2]/div[2]/label/span")).click();
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMovePartialInKindDiv']/div[2]/div/div/div[1]/div[1]/input")).sendKeys(assettype);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMovePartialInKindDiv']/div[2]/div/div/div[1]/div[2]/input")).sendKeys(nameofasset);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMovePartialInKindDiv']/div[2]/div/div/div[1]/div[3]/input")).sendKeys(quantity);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath(".//*[@id='transferRolloverAssetAlternativeMovePartialInKindDiv']/div[2]/div/div/div[1]/div[4]/input")).sendKeys(value);
										
				}
			
			//Channel of Movement
			if (myfunds.equals("Check/Mail"))
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferRolloverChannelOfMovement']/div[3]/div[1]/label/span")).click();
			} 
			else
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferRolloverChannelOfMovement']/div[3]/div[2]/label/span")).click();
			}
			if (rollover1.equals("Mail"))
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferRolloverChannelOfMovement']/div[4]/div[1]/label/span")).click();
			} 
			else if(rollover1.equals("Express mail via FedEx ($ fee)"))
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='transferRolloverChannelOfMovement']/div[4]/div[2]/label/span")).click();
				//Creditcard
				d.findElement(By.xpath(".//*[@id='creditCard']/div[3]/div[1]/label/span")).click();
			}
			else 
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/div[3]/div[2]/div/form/div[1]/div/div/div[1]/div[3]/div[6]/div[4]/div[4]/label/span")).click();
			}
			
			
			//Disclosures
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath(".//*[@id='transferRolloverChannelOfMovement']/div[4]/div[4]/label/span")).click();
			//Signature
			if (signature.equals("Electronic Signature")) 
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='signature']/div[3]/div[1]/label/span")).click();
			} 
			else if(signature.equals("Print, sign and upload document"))
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='signature']/div[3]/div[2]/label/span")).click();
			}
			else 
			{
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='signature']/div[3]/div[3]/label/span")).click();
			}
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("documentFormSubmitButton")).click();
			
			String expurl;
			expurl=d.getCurrentUrl();
			if (expurl.contains(expurl))
			{
				return true;
			}
			else 
			{
				return false;
			}

		}
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
		
}

