package com.ndira.client.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import com.ndira.client.portal.utils.NDira_clientporatal_Constants;
import com.ndira.client.portal.utils.NDira_clientporatal_Propertiesfile;
import com.ndira.client.portal.utils.NDira_clientporatal_XLUtils;

public class Loginlogoutmethod extends NDira_clientporatal_Constants
{			
		public static void mydirectionlogout() 
		{
			try 
			{
				d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
				d.findElement(By.linkText("Log out")).click();			
			} 
			catch (Exception e) 
			{
				System.out.println(e);
			}
	    }
		public void mydirectionLogin() throws IOException
		  {
			try
			{
				NDira_clientporatal_XLUtils xl=new NDira_clientporatal_XLUtils();
				String xlfile = NDira_clientporatal_Propertiesfile.getProperty("file.xlfile.path");
				String tcsheet="TestCases";
				String tssheet="TestSteps";
				  
				String uname =xl.getCellData(xlfile, tssheet, 1, 5);
				String pass =xl.getCellData(xlfile, tssheet, 1, 6);
				d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				d.findElement(By.id("login")).clear();
				d.findElement(By.id("login")).sendKeys(uname);
				d.findElement(By.id("password")).clear();
				d.findElement(By.id("password")).sendKeys(pass);
				d.findElement(By.id("submit-button")).click();
				d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
				String ques=d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/strong/h5")).getText();
				String ques1=xl.getCellData(xlfile, tssheet, 2, 5);
				if (ques.contains(ques1))
				{
					String ans1=xl.getCellData(xlfile, tssheet, 2, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans1);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques2=xl.getCellData(xlfile, tssheet, 3, 5);
				if (ques.contains(ques2))
				{
					String ans=xl.getCellData(xlfile, tssheet, 3, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques3=xl.getCellData(xlfile, tssheet, 4, 5);
				if (ques.contains(ques3))
				{
					String ans=xl.getCellData(xlfile, tssheet, 4, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques4=xl.getCellData(xlfile, tssheet, 5, 5);
				if (ques.contains(ques4))
				{
					String ans=xl.getCellData(xlfile, tssheet, 5, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques5=xl.getCellData(xlfile, tssheet, 6, 5);
				if (ques.contains(ques5))
				{
					String ans=xl.getCellData(xlfile, tssheet, 6, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques6=xl.getCellData(xlfile, tssheet, 7, 5);
				if (ques.contains(ques6))
				{
					String ans=xl.getCellData(xlfile, tssheet, 7, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques7=xl.getCellData(xlfile, tssheet, 8, 5);
				if (ques.contains(ques7))
				{
					String ans=xl.getCellData(xlfile, tssheet, 8, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques8=xl.getCellData(xlfile, tssheet, 9, 5);
				if (ques.contains(ques8))
				{
					String ans=xl.getCellData(xlfile, tssheet, 9, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques9=xl.getCellData(xlfile, tssheet, 10, 5);
				if (ques.contains(ques9))
				{
					String ans=xl.getCellData(xlfile, tssheet, 10, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques10=xl.getCellData(xlfile, tssheet, 11, 5);
				if (ques.contains(ques10))
				{
					String ans=xl.getCellData(xlfile, tssheet, 11, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques11=xl.getCellData(xlfile, tssheet, 12, 5);
				if (ques.contains(ques11))
				{
					String ans1=xl.getCellData(xlfile, tssheet, 12, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans1);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques12=xl.getCellData(xlfile, tssheet, 13, 5);
				if (ques.contains(ques12))
				{
					String ans1=xl.getCellData(xlfile, tssheet, 13, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans1);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques13=xl.getCellData(xlfile, tssheet, 14, 5);
				if (ques.contains(ques13))
				{
					String ans1=xl.getCellData(xlfile, tssheet, 14, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans1);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques14=xl.getCellData(xlfile, tssheet, 15, 5);
				if (ques.contains(ques14))
				{
					String ans1=xl.getCellData(xlfile, tssheet, 15, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans1);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
				String ques15=xl.getCellData(xlfile, tssheet, 16, 5);
				if (ques.contains(ques15))
				{
					String ans1=xl.getCellData(xlfile, tssheet, 16, 6);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/div/input")).sendKeys(ans1);
					Sleeper.sleepTightInSeconds(2);
					d.findElement(By.xpath("html/body/div[3]/div[2]/div/div[2]/div[5]/div/div/form/button")).click();
					
				}
			}
			catch (Exception e) 
			{
				System.out.println(e);
			}
			
		  }
	}
