package com.ndira.client.portal.libraries;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.client.portal.utils.NDira_clientporatal_Constants;

public class MyDirectionPortalClientSignupForm extends NDira_clientporatal_Constants
{
	public static String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount,investiment,phonen,cardno,cardExpiryDate,cardName,cardCVCNumber,AccNo,emailnotifications,Coupencode,Code,administrationfee,aaf,referal;
	public static boolean clientsignupform() 
	{
		try 
		{
			d.findElement(By.xpath("html/body/div[4]/div/div[2]/div[2]/div/div[1]/a")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("firstName")).sendKeys(fname);
			  
			  d.findElement(By.id("lastName")).sendKeys(lname);
			 
			  d.findElement(By.id("emailAddress")).sendKeys(email);
			  
			  d.findElement(By.id("password")).sendKeys(pass);
			  
			  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(2);
			 
			  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("socialSecurity")).sendKeys(SSN);
			  Sleeper.sleepTightInSeconds(2);
			  Select ms=new Select(d.findElement(By.id("maritalStatus")));
			  ms.selectByVisibleText(maritalstatus);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("phone")).sendKeys(phone);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("street")).sendKeys(address1);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("aptSte")).sendKeys(address2);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("city")).sendKeys(city);
			  Sleeper.sleepTightInSeconds(2);
			  Select st=new Select(d.findElement(By.id("state")));
			  st.selectByVisibleText(state);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("zip")).sendKeys(zipcode);
			  Sleeper.sleepTightInSeconds(2);
			  
			  if (emailnotifications.contains("Yes"))
			  {
				  Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/form/div[3]/div[2]/div[2]/label[1]/span")).click();
		   	  }
			  else 
			  {
				  Sleeper.sleepTightInSeconds(2);
				  d.findElement(By.xpath("html/body/form/div[3]/div[2]/div[2]/label[2]/span")).click();
			  }
			  
			  Select toa=new Select(d.findElement(By.id("accountType")));
			  toa.selectByVisibleText(typeofaccount);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/form/div[6]/div[2]/div[2]/div[1]/div[1]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/form/div[6]/div[2]/div[2]/div[2]/div[1]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/form/div[6]/div[2]/div[2]/div[3]/div[1]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/form/div[6]/div[2]/div[2]/div[1]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/form/div[6]/div[2]/div[2]/div[2]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/form/div[6]/div[2]/div[2]/div[3]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  //Annual Administration Fees
			  if (aaf.contains("Quantity Based"))
			  {
				  Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/form/div[7]/div/div[2]/table[1]/tbody/tr[3]/td/div/div[1]/div[1]/p/label/span[1]")).click();
		   	  }
			  else 
			  {
				  Sleeper.sleepTightInSeconds(2);
				  d.findElement(By.xpath("html/body/form/div[7]/div/div[2]/table[1]/tbody/tr[3]/td/div/div[2]/div[1]/p/label/span[1]")).click();
			  }
			  
			  d.findElement(By.xpath("html/body/form/div[8]/div[2]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(3);
			  
			/*  d.findElement(By.id("investmentAmount")).sendKeys(investiment);
			  Sleeper.sleepTightInSeconds(3);
			  d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[4]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(3);
			  d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  
			  d.findElement(By.id("contactMePhone")).sendKeys(phonen);
			  Sleeper.sleepTightInSeconds(3);
			  d.findElement(By.xpath("html/body/div[3]/form/div[3]/div[2]/div[2]/label[1]/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/div[3]/form/div[4]/div[2]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("nextButton")).click();*/
			  
			  
			  //Do you have a coupon code ?
			  if (Coupencode.contains("Yes"))
			  {
				  Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/form/div[2]/div[1]/div[2]/label[1]/span")).click();
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.id("couponCode")).sendKeys(Code);
		   	  }
			  else 
			  {
				  Sleeper.sleepTightInSeconds(2);
				  d.findElement(By.xpath("html/body/form/div[2]/div[1]/div[2]/label[2]/span")).click();
			  }
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("howDidYouHearOfUs")).sendKeys(referal);
			  //Payment Information
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("cardNumber")).sendKeys(cardno);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("cardExpiryDate")).sendKeys(cardExpiryDate);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("cardName")).sendKeys(cardName);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("cardCVCNumber")).sendKeys(cardCVCNumber);
			  Sleeper.sleepTightInSeconds(2);
			  
			  //How would you like to pay your administration fees?
			  if (administrationfee.contains("Withdraw fees from my account"))
			  {
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath("html/body/form/div[5]/div[2]/div[2]/label[1]/span")).click();
			  }
			  else 
			  {
				  Sleeper.sleepTightInSeconds(2);
				  d.findElement(By.xpath("html/body/form/div[5]/div[2]/div[2]/label[2]/span")).click();
			  }
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/form/div[7]/div[2]/div[2]/label/span[1]")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(10);
			  String expmsg;
			  expmsg=d.findElement(By.xpath("html/body/div[4]/div/div/div[2]/div/h4")).getText();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/div[4]/div/div/div[2]/div/div/div/div/div/div/div/a")).click();
			  if (expmsg.contains("SUCCESS!")) 
			  {
				return true;
			  } 
			  else
			  {
				  d.get(url);
				 return false;
			  }
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
